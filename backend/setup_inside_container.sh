#!/bin/bash
cd /root/my-table 
git pull 
mysql -uroot -ppassword </root/my-table/backend/sql_setup.txt;
cd /root/my-table/middleware
npm install
npm update
cd /root/my-table/frontend
npm install
npm update
npm run build
printf "\n\nDone! Now run the following:\n\tcd /root/my-table/middleware\n\tnode middleware.js &\n\n";