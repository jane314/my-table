version=$(cat config.json | jq -r '.version');
docker build -t mysql:mycustom$version .;
docker create -e MYSQL_ROOT_PASSWORD=password -p 8778:8778 --name mysqlcontainer$version mysql:mycustom$version;
docker container start mysqlcontainer$version;
printf "Done!\n";
printf "You can enter the Docker container with:\n\tdocker exec -it mysqlcontainer$version bash\n";
printf "Inside the container, you can start the DB server by running the following commands in order:\n\tcd ~/my-table/backend;\n\tbash ./setup_inside_container.sh\n\n";