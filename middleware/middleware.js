import express from 'express';
import mysql from 'mysql';

function sql_splice(arr, quotes) {
	if (quotes) {
		return `( ${arr.map((t) => `"${t}"`).join(',')} )`;
	} else {
		return `( ${arr.join(',')} )`;
	}
}

const app = new express();

app.use(express.static('/root/my-table/frontend/dist'));
const db_connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: 'password',
	database: 'test01',
});
db_connection.connect();
app.use(express.json());

app.get('/db/mytable', function (req, res) {
	console.dir(req.body);
	res.writeHead(200);
	db_connection.query(
		`select * from mytable;`,
		function (error, results, fields) {
			if (error) throw error;
			res.end(JSON.stringify(results));
		}
	);
});
app.post('/post', function (req, res) {
	console.dir(req.body);
	res.writeHead(200);
	db_connection.query(
		`insert into ${req.body.name} ${sql_splice(
			Object.keys(req.body.input),
			false
		)} values ${sql_splice(Object.values(req.body.input), true)};`,
		function (error, results, fields) {
			if (error) {
				throw error;
				res.end('not-ok');
			} else {
				res.end(JSON.stringify(results));
			}
		}
	);
	// res.end('ok');
});
app.post('/delete', function (req, res) {
	console.dir(req.body);
	res.writeHead(200);
	db_connection.query(
		`delete from ${req.body.name} where id=${req.body.id};`,
		function (error, results, fields) {
			if (error) {
				throw error;
				res.end('not-ok');
			} else {
				res.end(JSON.stringify({ id: req.body.id }));
			}
		}
	);
	// res.end('ok');
});

app.listen(8778);
