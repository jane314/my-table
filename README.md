# My Table

A nice little table that anyone can edit. I've tried to make it friendly and fun to use.

I'm providing the full stack here! The Dockerfile spins up a VM which runs an instance of "My Table".
